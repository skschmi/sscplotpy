# ss_cplot.py

CPLOT in Python - CS 557 - Steven Schmidt

* Ported from the original C by some cool guys in my class.
* Original code written by Dr. Tom Sederberg, BYU.
* Additional functionality added by Steven Schmidt, for the projects in the class (CS 557).

## Requirements

* python3
* numpy
* scipy

## Usage

* runs/run_proj1.sh
* runs/run_proj2.sh
* runs/run_proj3.sh
