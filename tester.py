import ss_cplot
from ss_cplot import Bspline
import numpy

bsp = Bspline(3, numpy.array([[0.,0,1],
                            [5,5,1],
                            [10,7,1],
                            [15,5,1],
                            [20.,0,1]]), numpy.array([1.,1,1,2,3,3,3]) )

print(bsp.knot_v)
print(bsp.points)
bsp.insertknot(2)
bsp.insertknot(2)
print(bsp.knot_v)
print(bsp.points)
