# Steven Schmidt
# CS 557 Projects
#  "cplot" written by Dr. Tom Sederberg,
#  ported to Python by two awesome guys in my class.
#  Added functions for the assignment written my me (Steven Schmidt)

import sys
import numpy
import scipy
from scipy import special

# 'a' is 0-based
def bernstein_basis(a,p,t,tmin=0.,tmax=1.):
    if(a < 1 or a > p+1):
        return 0.0
    tmapped = (t - tmin)/(tmax-tmin)
    result = special.binom(p,a) * tmapped**(a) * (1.-tmapped)**(p-a)
    return result

with open("sederstring.eps") as f:
    sederstring = f.read()

#Point is numpy.array([[x1*w1,y1*w1,w1],[x2*w2,y2*w2,w2],[x3*w3,y3*w3,w3],...])
class Bezier(object):
    def __init__(self, points):
        self.degree = len(points) - 1
        self.points = points

    def copy(self):
        return Bezier(self.points.copy())

class Bspline(object):
    def __init__(self, degree, points, knot_v):
        self.degree = degree
        self.points = points
        self.knot_v = knot_v

    def copy(self):
        return Bspline(self.degree,self.points.copy(),self.knot_v.copy())

    def insertknot(self,newknot):
        # inserts the given knot, and adjusts the points accordingly.
        n = len(self.points)
        m = len(self.points)+1
        kl = len(self.knot_v)
        kkl = len(self.knot_v)+1
        newpoints = numpy.zeros((m,3))
        newknotv = numpy.zeros(kkl)

        if(newknot < min(self.knot_v) or newknot > max(self.knot_v)):
            print("Error: knot out-of-bounds. 'Canknot' knot-insert. (ha ha. get it? 'can-knot'? LOL.)")
            exit()

        # inserts the knot
        new_i = 0
        old_i = 0
        knot_inserted = False
        knot_inserted_i = -1
        while(1):
            if(new_i >= kkl):
                break
            if( not knot_inserted and (old_i >= kl or newknot < self.knot_v[old_i]) ):
                newknotv[new_i] = newknot
                knot_inserted_i = new_i
                knot_inserted = True
                new_i += 1
            else:
                newknotv[new_i] = self.knot_v[old_i]
                new_i += 1
                old_i += 1

        # Computes the new control points
        a = 0
        for a in range(0,m):
            if(a == 0):
                newpoints[a] = self.points[a]
            elif( a > 0 and a < m-1 ):
                alpha = 0.0
                if(1 <= a and a <= (knot_inserted_i - self.degree)):
                    alpha = 1.0
                elif( (knot_inserted_i - self.degree + 1) <= a and a <= knot_inserted_i):
                    alpha = (newknot - self.knot_v[a-1])/(self.knot_v[a+self.degree-1] - self.knot_v[a-1])
                newpoints[a] = alpha*self.points[a] + (1-alpha)*self.points[a-1]
            elif( a == m-1 ):
                newpoints[a] = self.points[a-1]

        # finishing up
        self.knot_v = newknotv
        self.points = newpoints

    def multiplicity(self,knot):
        # returns the multiplicity of knot.
        count = 0
        for k in self.knot_v:
            if k == knot:
                count += 1
        return count


def extract_bezier_from_bspline(bspline,i):
    # Extract the i-th bezier curve from the bspline.

    # Copy the old bspline to a new one that we can modify.
    newbspline = bspline.copy()

    # Knot insert until there are exactly "degree" of each knot.
    for k in bspline.knot_v:
        while(newbspline.multiplicity(k) < bspline.degree):
            newbspline.insertknot(k)

    # ok, at this point the i-th bezier curve should be
    # control points 3*(i-1) + 1 ... 3*(i-1) + degree inclusive.
    bezpoints = numpy.zeros((bspline.degree+1,3))
    bez_j = 0
    for bsp_j in range( 3*(i-1), 3*(i-1) + bspline.degree + 1):
        bezpoints[bez_j,:] = newbspline.points[bsp_j,:]
        bez_j += 1

    return Bezier(bezpoints)


# Returns a new bezier that is degree-elevated.
def elevate(bezier):
    numpts = len(bezier.points)
    # Polynomial bezier points
    ppts = bezier.points
    # degree-elevated polynomial bezier points
    elev_ppts = numpy.zeros((numpts+1,3))
    elev_ppts[0] = ppts[0]
    elev_ppts[-1] = ppts[-1]
    for i in range(1,numpts):
        numer = float(bezier.degree + 1 - i)
        denom = float(bezier.degree + 1)
        elev_ppts[i] = ppts[i-1] + (numer/denom)*(ppts[i]-ppts[i-1])
    # degree-elevated rational bezier points
    elev_rwts = numpy.zeros((numpts+1,1))
    elev_rwts[:,0] = elev_ppts[:,2]
    elev_rpts = numpy.zeros((numpts+1,2))
    elev_rpts[:,0:2] = elev_ppts[:,0:2] / elev_rwts
    # Put new points into new bezier object
    newpoints = numpy.zeros((numpts+1,3))
    newpoints[:,0:2] = elev_rpts[:,0:2]
    newpoints[:,2] = elev_rwts[:,0]
    newbezier = Bezier(newpoints)
    # Return value
    return newbezier

# Returns two bezier curves, left and right, that together
# build the original bezier curve.
# Divided at original bezier curve's paramter t.
def subdivide(bezier,t):
    numpts = len(bezier.points)
    # Polynomial bezier points
    ppts = bezier.points.copy()

    # subdivide values
    s = []
    for i in range(bezier.degree+1,0,-1):
        s.append(ppts.copy())
        if(len(ppts)>=2):
            ppts = ppts[0:-1] + t*(ppts[1:] - ppts[0:-1])

    # Put the values into their respective bezier
    bezierleft = Bezier(numpy.zeros((numpts,3)))
    bezierright = Bezier(numpy.zeros((numpts,3)))
    for i in range(0,numpts):
        bezierleft.points[i] = s[i][0]
    for i in range(numpts-1,-1,-1):
        bezierright.points[numpts-i-1] = s[i][-1]

    # return values
    return bezierleft, bezierright

# Returns the x coordinate for paramter t on the bezier curve.
def x_for_t(bezier,t):
    c0,c1 = subdivide(bezier,t)
    if( t > 0.5 ):
        return c0.points[-1][0:2]
    return c1.points[0][0:2]

# Returns the curvature value at paramter t on the bezier curve.
def curv_for_t(bezier,t):
    c0,c1 = subdivide(bezier,t)
    if( t > 0.5 ):
        subcv = c0
        w0 = subcv.points[-1][2]
        w1 = subcv.points[-2][2]
        w2 = subcv.points[-3][2]
        p0 = subcv.points[-1][0:2]/w0
        p1 = subcv.points[-2][0:2]/w1
        p2 = subcv.points[-3][0:2]/w2
    else:
        subcv = c1
        w0 = subcv.points[0][2]
        w1 = subcv.points[1][2]
        w2 = subcv.points[2][2]
        p0 = subcv.points[0][0:2]/w0
        p1 = subcv.points[1][0:2]/w1
        p2 = subcv.points[2][0:2]/w2

    p0p1 = p1-p0
    p1p2 = p2-p1
    normperp = numpy.array([[0,1],[-1,0]]).dot(p0p1)
    normperp = normperp/numpy.linalg.norm(normperp)

    # Which side is "in" and which side is "out"
    sign = -1.0
    if( p0p1[0]*p1p2[1] - p0p1[1]*p1p2[0] < 0 ):
        sign = 1.0

    a = numpy.linalg.norm(p0p1)
    h = abs(p1p2.dot(normperp))
    curvature = (w0*w2/(w1**2)) * ((subcv.degree-1)/(subcv.degree)) * (h/(a**2))
    r = 1/curvature
    center = p0 + sign*r*normperp

    return curvature,center,p0


# Convert power basis coefficients to bernstein basis coefficients
def power_to_bernstein_coefs(fvec):
    degree = len(fvec)-1
    BtoP = numpy.zeros((degree+1,degree+1))
    # Constructing the basis conversion matrix
    for i in range(0,degree+1):
        for j in range(0,degree+1):
            BtoP[i,j] = special.binom(degree,i) * special.binom(i,j) * (-1)**(i-j)
    # Finding the bernstein basis coefficients
    bbvec = numpy.linalg.inv(BtoP).dot(fvec)
    return bbvec

def construct_bezier_from_powerpoly(fvec):
    bbvec = power_to_bernstein_coefs(fvec)
    xvec = numpy.linspace(0.,1.,len(fvec))
    newpoints = numpy.zeros((len(xvec),3))
    newpoints[:,2] = 1.0
    for i in range(0,len(xvec)):
        newpoints[i,0] = xvec[i]
        newpoints[i,1] = bbvec[i]
    thebezier = Bezier(newpoints)
    return thebezier

def compute_offset(bezier,t,radius):
    c0,c1 = subdivide(bezier,t)
    if( t > 0.5 ):
        subcv = c0
        w0 = subcv.points[-1][2]
        w1 = subcv.points[-2][2]
        w2 = subcv.points[-3][2]
        p0 = subcv.points[-1][0:2]/w0
        p1 = subcv.points[-2][0:2]/w1
        p2 = subcv.points[-3][0:2]/w2
    else:
        subcv = c1
        w0 = subcv.points[0][2]
        w1 = subcv.points[1][2]
        w2 = subcv.points[2][2]
        p0 = subcv.points[0][0:2]/w0
        p1 = subcv.points[1][0:2]/w1
        p2 = subcv.points[2][0:2]/w2

    p0p1 = p1-p0
    p1p2 = p2-p1
    normperp = numpy.array([[0,1],[-1,0]]).dot(p0p1)
    normperp = normperp/numpy.linalg.norm(normperp)

    # Which side is "in" and which side is "out"
    sign = -1.0
    if( p0p1[0]*p1p2[1] - p0p1[1]*p1p2[0] < 0 ):
        sign = 1.0

    offset = sign * radius * normperp
    return offset,p0


# Returns the string that needs to be written to the output to draw the bezier curve
def plotBezier(bezier):
    pointlines = ["[{} {} {}]".format(*tuple(p)) for p in bezier.points]
    return "[" + "".join(pointlines) + "] cplot"

# Returns the string that needs to be written to the output to draw the control polygon
def plotControlPolygon(bezOrNurbs):
    pointlines = ["{} {} mv ".format(*tuple(rationalPointCoord(bezOrNurbs.points[0])))]
    for pt in bezOrNurbs.points[1:]:
        pointlines.append("{} {} ln ".format(*tuple(rationalPointCoord(pt))))
    pointlines.append("stroke")
    return "".join(pointlines)

# Returns the x,y coordinates of the rational bezier
# curve point, given the polynomial bezier curve point.
def rationalPointCoord(polypt):
    return polypt[0]/polypt[2], polypt[1]/polypt[2]

# Converts rational bez curve point (x,y,w)
# to polynomial beizer curve point (x*w,y*w,w)
def rationalPointToPolyPoint(ratpt):
    return ratpt[0]*ratpt[2], ratpt[1]*ratpt[2], ratpt[2]

def main():
    print("Input arguments: ", sys.argv)
    if len(sys.argv) < 3:
        print("Syntax:\n python3 ss_cplot.py inputfilename outputfilename")
        return

    global curves, output
    curves = {}
    output = [sederstring]
    window_p0 = numpy.zeros(2)
    window_p1 = numpy.zeros(2)

    with open(sys.argv[1]) as f:
        print("Input file: ", sys.argv[1])
        while True:
            line = f.readline().strip().lower()[:4]
            print("Reading command: ",line)

            if line == "bord":
                output.append("border stroke")

            elif line == "cplo":
                nCurve = int(f.readline().strip().lower())
                output.append(plotBezier(curves[nCurve]))

            elif line == "circ":
                params = map(float, f.readline().strip().lower().split())
                output.append("{} {} {} circ".format(*params))

            elif line == "colo":
                # Set the color
                params = map(float, f.readline().strip().lower().split())
                output.append("{} {} {} setrgbcolor".format(*params))

            elif line == "cppl":
                # Plot the control polygon of the bezier curve
                nCurve = int(f.readline().strip().lower())
                output.append(plotControlPolygon(curves[nCurve]))

            elif line == "exit":
                break

            elif line == "stor":
                nCurve = int(f.readline().strip().lower())
                degree = int(f.readline().strip().lower())
                pts = []
                for v in range(degree+1):
                    pt_vals = f.readline().strip().lower().split()
                    for i in range(0,len(pt_vals)):
                        pt_vals[i] = float(pt_vals[i])
                    pts.append(rationalPointToPolyPoint(pt_vals))
                curves[nCurve] = Bezier(numpy.array(pts))

            elif line == "text":
                size, x, y = map(float, f.readline().strip().lower().split())
                text = f.readline().strip()
                output.append("{} {} mv /Times-Roman findfont {} scalefont setfont ({}) show".format(x,y,size,text))

            elif line == "view":
                params = map(float, f.readline().strip().lower().split())
                output.append("{} {} {} {} viewport".format(*params))

            elif line == "wind":
                params = f.readline().strip().lower().split()
                window_p0 = numpy.array([float(params[0]),float(params[1])])
                window_p1 = numpy.array([float(params[2]),float(params[3])])
                output.append("{} {} {} {} window".format(float(params[0]),float(params[1]),float(params[2]),float(params[3])))

            elif line == "widt":
                size = int(f.readline().strip().lower())
                output.append("{} setlinewidth".format(size))

            elif line == "disp":
                for i in sorted(curves):
                    c = curves[i]
                    for p in c.points:
                        pass

            elif line == "subd":
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                t = float(params[1])
                nLeft = int(params[2])
                nRight = int(params[3])
                curveLeft, curveRight = subdivide(curves[nCurve],t)
                curves[nLeft] = curveLeft
                curves[nRight] = curveRight

            elif line == "elev":
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                numelevate = int(params[1])
                nNewCurve = int(params[2])
                thecurve = curves[nCurve]
                for i in range(0,numelevate):
                    thecurve = elevate(thecurve)
                curves[nNewCurve] = thecurve

            elif line == "ccur":
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                n = int(params[1])
                # For each value of t, create a degree=1 curve (line)
                # and draw from the curve to the center-point of curvature
                for t in numpy.linspace(0.0,1.0,n+1):
                    newpoints = numpy.zeros((2,3))
                    newpoints[:,2] = 1
                    curv,center,x0 = curv_for_t(curves[nCurve],t)
                    newpoints[0,0:2] = x0
                    newpoints[1,0:2] = center
                    thecurve = Bezier(newpoints)
                    output.append(plotBezier(thecurve))

            elif line == "curv":
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                t = float(params[1])
                curv,center,p0 = curv_for_t(curves[nCurve],t)
                size = 9
                x = p0[0]
                y = p0[1]
                text = str(curv)
                output.append("{} {} mv /Times-Roman findfont {} scalefont setfont ({}) show".format(x,y,size,text))

            elif line == "comb":
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                n = 100
                # For each value of t, create a degree=1 curve (line)
                combcurvs = []
                for t in numpy.linspace(0.0,1.0,n+1):
                    newpoints = numpy.zeros((2,3))
                    newpoints[:,2] = 1
                    curv,center,x0 = curv_for_t(curves[nCurve],t)
                    newpoints[0,0:2] = x0
                    newpoints[1,0:2] = center
                    combcurvs.append((curv,center,x0))
                # figure out the maximum length
                cmax = 0.0
                for ccurv in combcurvs:
                    if abs(ccurv[0]) > cmax:
                        cmax = abs(ccurv[0])
                # Width of the window
                w_width = window_p1[1] - window_p0[1]
                # Ratio to shorten it by
                ratio = 0.2*w_width/cmax
                # Draw each line in the curve
                for ccurv in combcurvs:
                    center = ccurv[1]
                    curvature = ccurv[0]
                    x0 = ccurv[2]
                    newcenter = x0 - ratio*curvature*(center-x0)/numpy.linalg.norm(center-x0)
                    newpoints = numpy.zeros((2,3))
                    newpoints[:,2] = 1
                    newpoints[0,0:2] = x0
                    newpoints[1,0:2] = newcenter
                    thecurve = Bezier(newpoints)
                    output.append(plotBezier(thecurve))

            elif line == "expl":
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                n = int(params[1])
                fparams = f.readline().strip().lower().split()
                fvec = numpy.zeros(len(fparams))
                for i in range(0,len(fvec)):
                    fvec[i] = float(fparams[i])
                newcurve = construct_bezier_from_powerpoly(fvec)
                curves[nCurve] = newcurve

            elif line == "offs":
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                radius = float(params[1])
                n = 50
                i = 0
                old_offset = numpy.zeros(2)
                old_x0 = numpy.zeros(2)
                for t in numpy.linspace(0.0,1.0,n+1):
                    offset,x0 = compute_offset(curves[nCurve],t,radius)
                    if(i>0):
                        newpoints = numpy.zeros((2,3))
                        newpoints[:,2] = 1
                        newpoints[0,0:2] = old_x0+old_offset
                        newpoints[1,0:2] = x0+offset
                        thecurve = Bezier(newpoints)
                        output.append(plotBezier(thecurve))
                    old_offset = offset
                    old_x0 = x0
                    i += 1

            elif line == "stcn":
                # Store a cubic NURBS curve
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                degree = 3
                nPts = int(params[1])
                print("nCurve, degree, nPts",nCurve, degree, nPts)
                knot_v_str_array = f.readline().strip().lower().split()
                knot_v = []
                for kv_str in knot_v_str_array:
                    knot_v.append(float(kv_str))
                    print(knot_v)
                pts = []
                for v in range(nPts):
                    pt_vals = f.readline().strip().lower().split()
                    for i in range(0,len(pt_vals)):
                        pt_vals[i] = float(pt_vals[i])
                    print("pt_vals",pt_vals)
                    pts.append(rationalPointToPolyPoint(pt_vals))
                curves[nCurve] = Bspline(degree,numpy.array(pts),numpy.array(knot_v))

            elif line == "pncp":
                # Plot the control polygon of the Nurbs curve
                nCurve = int(f.readline().strip().lower())
                output.append(plotControlPolygon(curves[nCurve]))

            elif line == "nebz":
                # For cubic NURBS n, extract the i-th Bezier curve and
                # store it in Bezier curve number m.
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                iBez = int(params[1]) # 1-based index of the bezier curve in the B-spline
                mCurve = int(params[2]) # The index to save the bezier curve to.
                bezCurve = extract_bezier_from_bspline(curves[nCurve],iBez)
                curves[mCurve] = bezCurve

            elif line == "ikno":
                # Insert a knot into NURBS n at knot value t. Store the result in NURBS m.
                params = f.readline().strip().lower().split()
                nCurve = int(params[0])
                knot = float(params[1])
                mCurve = int(params[2])
                newBspline = curves[nCurve].copy()
                newBspline.insertknot(knot)
                curves[mCurve] = newBspline

            else:
                raise Exception("Illegal command: {}".format(line))

        with open(sys.argv[2], 'w') as f:
            print("Output file: ", sys.argv[2])
            f.write("\n".join(output)+"\n")

if __name__ == "__main__":
    main()
